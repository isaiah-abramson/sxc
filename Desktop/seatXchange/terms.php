<?php

    $js = '
    			<script src="https://use.fontawesome.com/a766d7656d.js"></script>
			';

	$fonts = '
				<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato" />
				<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
			';

	$facebookLink	= "https://www.facebook.com/sharer/sharer.php?u=http%3A//www.seatxchange.com%2F%3Freferral%3D$refCode%26refSource%3Dfacebook";
	$twitterLink 	= "https://twitter.com/home?status=http%3A//www.seatxchange.com%2F%3Freferral%3D$refCode%26refSource%3Dtwitter";
	$gplusLink		= "https://plus.google.com/share?url=http://www.seatxchange.com%2F%3Freferral%3D$refCode%26refSource%3Dgoogle";
	$shareLink		= "mailto:?subject=I%20thought%20you%20might%20like%20this&body=It%27s%20a%20new%20ticketing%20app%20for%20games%20and%20concerts%20that%27s%20going%20to%20be%20a%20game%20changer%21%20Check%20it%20out%20at%20http%3A%2F%2Fwww.seatxchange.com%2F%3Freferral%3D$refCode%26refSource%3Dmail";

	$bodyClass = "";

	$loader = false;

	$googleTrack = false;

	$login = "no";

	$overflow = '';

	include_once("pages/header.php");

?>
<div class="terms">
	<div class="terms-title">
		Terms &amp; Conditions
	</div>
	<div class="divider"></div>
	<article class="term">
		<div class="term-title">
			1. ELIGIBILITY:
		</div>
		<div class="term-desc">
			This Campaign is open only to those who sign up at <a href="https://www.seatxchange.com">www.seatXchange.com</a> or who sign up through a seatXchange street team brand ambassador and who are at least 18 years old as of the date of entry.

			The Campaign is open to all legal residents of California, and is void where prohibited by law.

			Employees of GrowthRocks, Viral-loops and SXC Live Inc. dba seatXchange (“SXC”), its affiliates, subsidiaries, advertising and promotion agencies, and suppliers, (collectively the “Employees”), and immediate family members and/or those living in the same household of employees are not eligible to participate in the campaign. The campaign is subject to all applicable federal, state, and local laws and regulations. Void where prohibited.
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			2. AGREEMENT TO RULES:
		</div>
		<div class="term-desc">
			By participating, the contestant (“you”) agree to be fully unconditionally bound by these rules, and you represent and warrant that you meet the eligibility requirements. In addition, you agree to accept the decisions of SXC as final and binding as it relates to the content of this campaign.
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			3. CAMPAIGN PERIOD:
		</div>
		<div class="term-desc">
			Entries will be accepted online starting on June 7, 2017 12:00 PM PDT and ending August 31, 2017 10:00 PM PDT. SXC may extend the deadline at its discretion. All online entries must be received by August 31, 2017 10:00 PM PDT, or if SXC has extended the Campaign Period, before the expiration of the Campaign Period
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			4. HOW TO ENTER:
		</div>
		<div class="term-desc">
			The Campaign may be entered either by submitting an entry using the online form provided at <a href="https://www.seatxchange.com">www.seatXchange.com</a> or by registering with a SXC street team brand ambassador. The entry must fulfill all Campaign requirements, as specified, to be eligible to earn a Reward. Entries that are incomplete or do not adhere to the rules or specifications may be disqualified at the sole discretion of SXC. You may enter only once. You must provide the information requested. You may not enter more times than indicated by using multiple email addresses, identities, or devices in an attempt to circumvent these rules. If You use fraudulent methods or otherwise attempt to circumvent these Rules, Your submission may be removed from eligibility at the sole discretion of SXC.
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			5. REWARDS:
		</div>
		<div class="term-desc">
			The winner(s) of the Campaign (the “Winner”) will receive the following Rewards:
			<ul>
				<li>
					<strong>Reward Level 1: </strong>Once five (5) Xfans register with your referral code you will receive $25 in exchange fee credit on the seatXchange app.
				</li>
				<li>
					<strong>Reward Level 2: </strong>Once ten (10) Xfans register with your referral code you will receive $20 off Your first exchange transaction on the seatXchange app.
				</li>
				<li>
					<strong>Reward Level 3: </strong>Once twenty-five (25) Xfans register with your referral code you will receive a First Access Invite for you and a friend to an upcoming Beta Test Event and Blockbuster After-Party.
				</li>
				<li>
					<strong>Reward Level 4: </strong>Once fifty (50) Xfans register with your referral code you will receive an invite for you and a friend to the seatXchange Red Carpet Fan Appreciation Event where you can mingle with celebrities while enjoying an open bar, hors d’oeuvres, and surprise performances.
				</li>
				<li>
					<strong>Reward Level 5: </strong>Once one hundred (100) Xfans register with your referral code you will receive a VIP invite with private transportation for you and a friend to the seatXchange Red Carpet Fan Appreciation Event along with an invite to the exclusive Fan Appreciation After Party.
				</li>
			</ul>
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			6. ADDITIONAL REWARD INFORMATION:
		</div>
		<div class="term-desc">
			The specifics of the reward(s) shall be solely determined by SXC. No cash or other reward(s) substitution shall be permitted except at SXC’s discretion. The reward(s) is nontransferable. Any and all reward-related expenses, including without limitation any and all federal, state, and/or local taxes, shall be the sole responsibility of the winner(s). No substitution of reward(s) or transfer/assignment of reward to others or request for the cash equivalent by winner is permitted. Acceptance of the reward constitutes permission for SXC to use Winner’s name, likeness, and entry for purposes of advertising and trade without further compensation, unless prohibited by law.
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			7. ODDS:
		</div>
		<div class="term-desc">
			If you satisfy the criteria for the particular reward level, you will receive the reward so long as you comply with the rules and procedures set forth herein to claim your reward.
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			8. WINNER NOTIFICATION:
		</div>
		<div class="term-desc">
			After You pre-register on <a href="https://www.seatxchange.com/">www.seatXchange.com</a>, the next time you login using your email address, you will see a display with the reward(s) you have earned and indication of how far you are from earning the next reward. With respect to reward levels 1, 2, 4, and 5, SXC will notify you by email when the seatXchange app is available on the App Store. Once you receive that notification, please download the seatXchange app and register using the same email address you used to pre-register on <a href="https://www.seatxchange.com/">www.seatXchange.com</a>. After you have completed your account creation on the seatXchange app, please respond to the SXC notification email you received and indicate that you have registered your account. Once SXC has confirmed your registration, SXC will credit your account with the respective reward(s). With respect to reward level, after you earn this reward, SXC will send you an email inviting you to the Beta Test Group and the next scheduled Beta Test Event and After Party. SXC shall have no liability for winner’s failure to receive notices due to spam, junk e-mail or other security settings. If winner cannot be contacted, is ineligible, or fails to claim the reward(s) within a reasonable time period, the winner’s reward(s) may be forfeited at SXC’s discretion. Receipt by winner of the Reward(s) offered in this Campaign is conditioned upon compliance with any and all federal, state, and local laws and regulations. ANY VIOLATION OF THESE OFFICIAL RULES BY WINNER (AT SXC Live Inc. dba SEATXCHANGE‘S SOLE DISCRETION) WILL RESULT IN WINNER’S DISQUALIFICATION AS WINNER OF THE CAMPAIGN, AND ALL PRIVILEGES AS WINNER WILL BE IMMEDIATELY TERMINATED.
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			9. RIGHTS GRANTED BY YOU:
		</div>
		<div class="term-desc">
			By entering this agreement, you understand and agree that SXC, anyone acting on behalf of SXC, and SXC’s licensees, successors, and assigns, shall have the right, where permitted by law, to print, publish, broadcast, distribute, and use in any media now known or hereafter developed, in perpetuity and throughout the world, without limitation, your entry, name, portrait, picture, voice, likeness, image, statements about the campaign, and biographical information for news, publicity, information, trade, advertising, public relations, and promotional purposes, without any further compensation, notice, review, or consent. If the content of your entry is claimed to constitute infringement of any proprietary or intellectual proprietary rights of any third party, you shall, at your sole and expense, defend or settle against such claims. You shall indemnify, defend, and hold harmless SXC from and against any suit, proceedings, claims, liability, loss, damage, costs or expense, which SXC may incur, suffer, or be required to pay arising out of such infringement or suspected infringement of any third party’s right.
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			10. TERMS & CONDITIONS:
		</div>
		<div class="term-desc">
			SXC reserves the right, in its sole discretion, to cancel, terminate, modify or suspend the campaign should virus, bug, non-authorized human intervention, fraud, or other cause beyond SXC’s control corrupt or affect the administration, security, fairness, or proper conduct of the Campaign. In such case, SXC may select the Winner from all eligible entries received prior to and/or after (if appropriate) the action taken by SXC. SXC reserves the right, in its sole discretion, to disqualify any individual who tampers or attempts to tamper with the entry process or the operation of the Campaign or website or violates these Terms &amp; Conditions. SXC has the right, in its sole discretion, to maintain the integrity of the campaign, to void votes for any reason, including, but not limited to: multiple entries from the same user from different IP addresses; multiple entries from the same computer in excess of that allowed by campaign rules; or the use of bots, macros, scripts, or other technical means for entering. Any attempt by an entrant to deliberately damage any website or undermine the legitimate operation of the campaign may be a violation of criminal and civil laws. Should such attempt be made, SXC reserves the right to seek damages to the fullest extent permitted by law.
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			11. LIMITATION OF LIABILITY:
		</div>
		<div class="term-desc">
			By entering, you agree to release and hold harmless SXC and its subsidiaries, affiliates, advertising and promotion agencies, partners, representatives, agents, successors, assigns, employees, officers, and directors from any liability, illness, injury, death, loss, litigation, claim, or damage that may occur, directly or indirectly, whether caused by negligence or not, from: (i) such entrant’s participation in the Campaign and/or his/her acceptance, possession, use, or misuse of any reward or any portion thereof; (ii) technical failures of any kind, including but not limited to the malfunction of any computer, cable, network, hardware, or software, or other mechanical equipment; (iii) the unavailability or inaccessibility of any transmissions, telephone, or internet service; (iv) unauthorized human intervention in any part of the entry process or the promotion; (v) electronic or human error in the administration of the campaign or the processing of entries.
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			12. DISPUTES:
		</div>
		<div class="term-desc">
			This campaign is governed by the laws of the Los Angeles, California, without respect to conflict of law doctrines. As a condition of participating in this campaign, participant agrees that any and all disputes that cannot be resolved between the parties, and causes of action arising out of or connected with this campaign, shall be resolved individually, without resort to any form of class action, exclusively before a court located in Los Angeles, California having jurisdiction. Further, in any such dispute, under no circumstances shall participant be permitted to obtain awards for, and hereby waives all rights to, punitive, incidental, or consequential damages, including reasonable attorney’s fees, other than participant’s actual out-of- pocket expenses (i.e. costs associated with entering this Campaign). Participant further waives all rights to have damages multiplied or increased.
		</div>
	</article>
	<article class="term">
		<div class="term-title">
			13. PRIVACY POLICY:
		</div>
		<div class="term-desc">
			The privacy and security of your information is very important to us. By using the <a href="https://www.seatxchange.com">www.seatXchange.com</a> you agree to the following privacy provisions:<br/><br/>

			<strong>A. The Information We Collect and Use</strong><br/><br/>

			i. Non-Personally Identifiable Information: When you provide information through our landing page or otherwise use or visit the website, we may collect certain data that does not tell us specifically who you are. This is “Non-Personally Identifiable Information.” It includes things like your Internet Protocol (IP) address, browser type, and the last domain you visited before coming to our website or the domain you go to when you leave. It also includes various statistical data such as which pages You visit on our websites, how long you stay on them, and what you click.<br/><br/>

			We may place a “cookie” on your computer. A cookie is a small amount of data, which often includes an anonymous unique identifier, sent to your browser from a website’s computers and stored on your computer’s hard drive. Our “preference” cookie expires after thirty (30) days. There is a simple procedure in most browsers that allows You to deny or accept cookies. <br/><br/>

			We also may use a small piece of code (sometimes referred to as “1x1 clear pixel,” “Web beacon” or “clear GIF”) placed in the pages of our websites to help us gather additional information about which parts of the site receive the most visitors and other user traffic patterns, and enable us to administer the Site. <br/><br/>

			No Personally Identifiable Information (see below) is collected through our cookies or Web beacons however we may, from time to time, allow advertisers, third-party advertising networks, and third-party advertising serving companies to serve advertisements directly to You within the Site, and related software. By serving these advertisements directly to You, these companies can set their own cookies on Your computer, which may not expire, and trigger their own Web beacons. If You would like to contact the advertisers, third-party advertising networks, and third-party advertising serving companies with which we have relationships to learn more about their privacy policies and what options (if any) they offer to opt out of their data collection, please visit their websites.<br/><br/>

			ii. Personally Identifiable Information. We also may collect information that can be used to identify you (“Personally Identifiable Information”). We only collect Personally Identifiable Information when You specifically provide it to us. For example, as part of our landing page You may be asked to provide information in order to receive updates from us and provide certain information including Your name, email addresses, address, and/or telephone number.<br/><br/>

			We may use Personally Identifiable Information to fulfill your requests, respond to Your inquiries and provide other services, and to alert You to information, offers, or publications in which we think You may be interested. The emails that we send contain links that enable You to “unsubscribe” from such mailings.<br/><br/> 

			We do not sell Personally Identifiable Information or other information You make available through use of the Site, or share such information with third parties, except as described below.<br/><br/> 

			From time to time, we may partner with unaffiliated companies or individuals for market research, product development or similar purposes. These companies or individuals may be provided with access to Personally Identifiable Information and Non-Personally Identifiable Information, but we will require by contract that they agree to maintain the confidentiality, security and integrity of such information. We also may subcontract with other companies and individuals to do work on our behalf; they may be provided with access to Personally Identifiable and Non-Personally Identifiable Information, but only as needed to perform their functions.<br/><br/>

			If you connect to the <a href="https://www.seatxchange.com">www.seatXchange.com</a> using credentials from a third party application, website or service (e.g., Facebook) (“Third Party Applications”), you authorize us to collect your authentication information, such as your username and encrypted access credentials. We may also collect other information available on or through your third party application account, including, for example, your name, profile picture, country, hometown, email address, date of birth, gender, friends’ names and profile pictures, and networks. These third party applications may have their own terms and conditions of use and privacy policies SXC does not endorse and is not responsible or liable for the behavior, features, or content of any third party application or for any transaction you may enter into with the provider of any such third party applications. <br/><br/>

			We may disclose Your Personally Identifiable Information to third parties in connection with a corporate transaction where all or a portion of our business (e.g., a portion that includes our customer lists) is sold or transferred. We also may disclose your Personally Identifiable Information if disclosure is required by law or as part of a lawsuit or government investigation or proceeding, or in order to permit us to exercise or preserve our legal rights or take action regarding potentially illegal activities or to protect the safety of any person.<br/><br/>

			<strong>B. Data Security</strong><br/><br/>

			SXC cannot ensure or warrant the security or privacy of any information you transmit to us. We attempt to follow policies and procedures that will protect against unauthorized access to your Personally Identifiable Information. But we can make no guarantees or promises in this regard, and You provide information to us at your own risk.<br/><br/>

			We may store the Personally Identifiable Information we hold about you indefinitely. We may store this information in the United States or we may transfer it to any other country. The data protection laws of the countries where we store Your personal data may or may not provide a level of protection equivalent to the laws in your home country. Wherever we hold the data, we will apply the terms of this privacy policy.<br/><br/>

			<strong>C. Children</strong><br/><br/> 

			Neither <a href="https://www.seatxchange.com">www.seatXchange.com</a> or the seatXchange app are directed at or intended for children under the age of 13 and we endeavor not to knowingly collect any Personally Identifiable Information from children under the age of 13. If a parent or guardian believes that the Site has collected the Personally Identifiable Information of a child under the age of 13, please contact <a href="mailto:contact@seatxchange.com">contact@seatXchange.com</a>.<br/><br/>

			<strong>D. Access to Your information</strong><br/><br/>

			You may contact us at <a href="mailto:contact@seatxchange.com">contact@seatXchange.com</a> if at any time You would like to see the Personally Identifiable Information we hold about You, or to ask us to correct or update this information, or to ask us to delete it. Please contact us if You have questions or wish to take any action with respect to information to which this Privacy Policy applies (see below for contact information).<br/><br/>

			California Privacy Rights. If you are a user of <a href="https://www.seatxchange.com">www.seatXchange.com</a> or the seatXchange app and a California resident, California’s “Shine the Light” law (California Civil Code § 1798.83) permits You to request and obtain from us once a year, free of charge, information regarding our disclosure of Your Personal Information (if any) to third parties for direct marketing purposes. If this law applies to you and you would like to make such a request, please submit your request in writing to the address provided in Section 6 below.<br/><br/>

			<strong>E. Links to Other Websites</strong><br/><br/>

			These terms & conditions and privacy policy apply only to <a href="https://www.seatxchange.com">www.seatXchange.com</a> and the seatXchange app. Websites and applications related to the SXC may contain links to other websites that SXC may not own or operate. The links from <a href="https://www.seatxchange.com">www.seatXchange.com</a> or the seatXchange app do not imply that SXC endorses or has reviewed these websites. The policies and procedures we describe here do not apply to these websites. We neither can control nor are responsible for the privacy practices or content of these websites. We suggest contacting these websites directly for information on their privacy policies.<br/><br/>

			<strong>F. How To Contact Us</strong><br/><br/>

			You may contact SXC regarding our privacy practices at the following address:<br/><br/> 

			SXC Live Inc. DBA seatXchange <br/>
			11601 Wilshire Blvd., Suite 500 <br/>
			Los Angeles, California 90025 <br/>
			<a href="mailto:contact@seatxchange.com">contact@seatXchange.com</a><br/>
			310.235.1407
		</div>
	</article>
</div>

<?php

	include_once("pages/footer.php");